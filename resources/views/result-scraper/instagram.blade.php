@extends('layouts.master')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Collecting Result</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Collecting Result
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Collecting Result</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-primary mb-2" type="button" data-toggle="modal"
                                data-target="#modalImport">Import Instagram Profil</button>
                            <a href="#" onclick="exportData()" class="btn btn-success mb-2">
                                <span class="icon-printer"></span>
                                Export</a>
                            <div class="row justify-content-between">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><b>Filter Tanggal</b></label>
                                        <input type="text" class="form-control" name="daterange" />
                                    </div>
                                </div>
                                <div class="form-group mr-3">
                                    <label><input onclick="addToAllInvestor()" type="checkbox" /> <b>Add All to Investor
                                            List</b></label>
                                </div>
                            </div>
                            <div class="table-responsive">
                                @if (Session::has('success'))
                                    <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert">
                                        <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong>Berhasil</strong> {{ Session::get('success') }}
                                    </div>
                                @endif
                                <table id="tableCommingSoon" class="no-style-no no-style">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Lengkap</th>
                                            <th>Email</th>
                                            <th>No Telepon</th>
                                            <th>Scrapered At</th>
                                            <th>Collected At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>

                            {{-- Modal Import --}}
                            <div class="modal fade text-left" id="modalImport" tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Import Profil</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" enctype="multipart/form-data"
                                                action="{{ url('result-scraper/import-data-instagram') }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label>File Profil</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="profilFile" class="custom-file-input"
                                                            id="customFile">
                                                        <label class="custom-file-label" for="customFile">Pilih
                                                            File</label>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-primary">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var tglAwal = "";
        var tglAkhir = "";
        $('input[name="daterange"]').daterangepicker({
            opens: 'right',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf(
                    'month')]
            }
        }, function(start, end, label) {
            tglAwal = start.format('YYYY-MM-DD');
            tglAkhir = end.format('YYYY-MM-DD');
            $("canvas#scraperReport").remove();
            $("div.chartreport").append('<canvas id="scraperReport" height="120"></canvas>');
            $("#tableCommingSoon").DataTable().clear().destroy();
            loadData(tglAwal, tglAkhir);
        });


        loadData(tglAwal, tglAkhir);

        function loadData(startDate, endDate) {
            var tableData = $("#tableCommingSoon").DataTable({
                ajax: '{{ url('result-scraper/fetch-data-instagram') }}' + '?startDate=' + startDate + '&endDate=' +
                    endDate,
                responsive: true,
                processing: true,
                serverSide: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                order: [
                    [0, "asc"]
                ],
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "full_name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "phone_number"
                    },
                    {
                        data: "scrapered_at"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "btnAksi"
                    },
                ]
            });
        }

        function detail(id) {
            $.ajax({
                url: "{{ url('penerbit/fetch-profil-follwer-ig') }}" + "/" + id,
                type: "GET",
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(res) {
                    const data = res.data;
                    $("#loader").hide();
                    $("#modalDetail").modal("show");
                    $("#titleDetailFollower").html("Detail " + data.profil_name);
                    $("#bio").html(data.bio);
                    $("#follower").html(data.follower);
                    $("#following").html(data.following);
                    $("#akunPrivate").html(data.is_private == "0" ? "Tidak" : "Ya");
                    $("#akunTerverifikasi").html(data.is_verrified == "0" ? "Tidak" : "Ya");
                }
            });
        }

        function exportData() {
            var url = "{{ url('result-scraper/export-data-instagram') }}" + '?start_date=' + tglAwal +
                '&end_date=' + tglAkhir;
            window.open(url, "_blank");
        }

        function addInvestor(id) {
            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah yakin menambahkan ke calon investor ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url('result-scraper/add-to-investor') }}' + '/' + id,
                        type: 'POST',
                        beforeSend: function() {
                            Swal.fire({
                                title: 'Please wait...',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                }
                            });
                        },
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sukses',
                                    text: res.message,
                                });
                                $("#tableCommingSoon").DataTable().clear().destroy();
                                loadData(tglAwal, tglAkhir);
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: res.message,
                                });
                            }
                        }
                    })
                }
            });
        }

        function addToAllInvestor() {
            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah yakin menambahkan ke semua investor ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url('result-scraper/add-to-all-investor') }}',
                        type: 'POST',
                        beforeSend: function() {
                            Swal.fire({
                                title: 'Please wait...',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                }
                            });
                        },
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sukses',
                                    text: res.message,
                                });
                                $("#tableCommingSoon").DataTable().clear().destroy();
                                loadData(tglAwal, tglAkhir);
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: res.message,
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                            console.log(errorThrown);
                            if (textStatus === "timeout" || textStatus === "error") {
                                $("#loader").hide();
                                Swal.fire({
                                    title: 'Ooops...',
                                    text: "Mohon periksa koneksi internet anda",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: 'Muat ulang',
                                    cancelButtonText: 'Tutup'
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                    })
                }
            });
        }

        function removeInvestor(id) {
            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah yakin menghapus dari calon investor ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url('result-scraper/remove-to-investor') }}' + '/' + id,
                        type: 'POST',
                        beforeSend: function() {
                            Swal.fire({
                                title: 'Please wait...',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                }
                            });
                        },
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sukses',
                                    text: res.message,
                                });
                                $("#tableCommingSoon").DataTable().clear().destroy();
                                loadData(tglAwal, tglAkhir);
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: res.message,
                                });
                            }
                        }
                    });
                }
            });

        }
    </script>
@endpush
