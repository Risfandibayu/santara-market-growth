@extends('layouts.master')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Instagram Profil Collector</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Instagram Profil Collector
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $data != null ? $data[0]->name : '' }}</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div>
                                <h5>Isi url follower instagram penerbit di file google sheet dibawah ini, lalu klik tombol
                                    mulai
                                    collecting.</h5>
                                @if (env('CONFIG_DEV') == "DEV")
                                    <a target="_blank"
                                        href="https://docs.google.com/spreadsheets/d/1WLGOEjPOE1Msy7yScGmvQV0mdMwvhm97EgTqQexYvRo/edit#gid=0">https://docs.google.com/spreadsheets/d/1WLGOEjPOE1Msy7yScGmvQV0mdMwvhm97EgTqQexYvRo/edit#gid=0</a>
                                @else
                                    <a target="_blank"
                                        href="https://docs.google.com/spreadsheets/d/1z4QykuDK5lO9zPaJwsULpH_KiIDHnGn1kFWLAS3T7SI/edit#gid=0">https://docs.google.com/spreadsheets/d/1z4QykuDK5lO9zPaJwsULpH_KiIDHnGn1kFWLAS3T7SI/edit#gid=0</a>
                                @endif
                            </div>
                            @if ($data != null)
                                <button type="button" onclick="mulaiScraping('{{ $data[0]->id }}')" id="btnSubmit"
                                    class="mt-2 btn btn-primary">Mulai
                                    Collecting</button>
                            @else
                                <button type="button" class="mt-2 btn btn-secondary">Profil instagram belum diatur di
                                    phantombuster</button>
                            @endif
                            <button class="mt-2 btn btn-primary hidden" id="btnLoading" type="button" disabled>
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Proses Collecting...
                            </button>
                            <br />
                            <div class="mt-2" id="resultScrapper"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let monitoringState = false;
        let containerId = "";

        function mulaiScraping(id) {
            $.ajax({
                url: "{{ url('launch-scraper-ig-phantombuster') }}" + "/" + id,
                type: "POST",
                beforeSend: function() {
                    $("#btnSubmit").addClass("hidden");
                    $("#btnLoading").removeClass("hidden");
                },
                success: function(response) {
                    if (response.data.containerId == undefined) {
                        $("#btnSubmit").removeClass("hidden");
                        $("#btnLoading").addClass("hidden");
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: response.data,
                        });
                    } else {
                        containerId = response.data.containerId;
                        console.log(response.data.containerId);
                        fetchContainerID(response.data.containerId);
                        monitoringState = true;
                        console.log(monitoringState);
                    }
                }
            });
        }

        setInterval(() => {
            if (monitoringState) {
                fetchContainerID(containerId);
            }
        }, 10000);

        function fetchContainerID(id) {
            $.ajax({
                url: "{{ url('fetch-container-phantombuster') }}" + "/" + id,
                type: "GET",
                success: function(response) {
                    if (response.data.status == undefined) {
                        $("#btnSubmit").removeClass("hidden");
                        $("#btnLoading").addClass("hidden");
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: response.data,
                        });
                    } else {
                        if (response.data.status == "running") {
                            fetchResultContainerID(response.data.id);
                        } else if (response.data.status == "finished") {
                            monitoringState = false;
                            fetchResultContainerID(response.data.id);
                        }
                    }
                }
            });
        }

        function fetchResultContainerID(id) {
            $.ajax({
                url: "{{ url('result-container-phantombuster') }}" + "/" + id,
                type: "GET",
                success: function(response) {
                    if (response.data.resultObject != null) {
                        let resultData = JSON.parse(response.data.resultObject);
                        console.log(resultData);
                        if (resultData[0] != null) {
                            const csvData = objectToCsv(resultData);
                            let link = document.createElement('a')
                            link.id = 'download-csv'
                            link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(
                                csvData));
                            link.setAttribute('download', 'hasil_profil_scraping.csv');
                            document.body.appendChild(link)
                            document.querySelector('#download-csv').click();
                        } else {
                            $("#resultScrapper").html(
                                '<h5>Klik disini untuk download daftar profil follower. <a href="' +
                                resultData.csvURL + '" target="_blank">' + resultData.csvURL + '</a></h5>');
                        }
                    } else {
                        if (!monitoringState) {
                            $("#resultScrapper").html(
                                '<h5 class="text-danger">Data tidak ditemukan masuk ke akun phantombuster, pastikan session cookies masih terhubung Intagram</h5>'
                            );
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.data,
                            });
                        }
                    }
                    if (!monitoringState) {
                        $("#btnSubmit").removeClass("hidden");
                        $("#btnLoading").addClass("hidden");
                    }
                }
            });
        }

        const objectToCsv = function(data) {

            const csvRows = [];
            const headers = Object.keys(data[0]);

            csvRows.push(headers.join(','));

            for (const row of data) {
                const values = headers.map(header => {
                    const val = row[header]
                    return `"${val}"`;
                });

                csvRows.push(values.join(','));
            }

            return csvRows.join('\n');
        };
    </script>
@endpush
