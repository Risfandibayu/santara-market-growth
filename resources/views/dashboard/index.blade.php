@extends('layouts.master')
@section('content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Selamat Datang, {{ Auth::user()->name }}</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Filter Tanggal</label>
                                    <input type="text" class="form-control" name="daterange" />
                                </div>
                            </div>
                            <div class="chartreport">
                                <canvas id="scraperReport" height="120"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var tglAwal = "";
        var tglAkhir = "";
        $('input[name="daterange"]').daterangepicker({
            opens: 'right',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf(
                    'month')]
            }
        }, function(start, end, label) {
            tglAwal = start.format('YYYY-MM-DD');
            tglAkhir = end.format('YYYY-MM-DD');
            $("canvas#scraperReport").remove();
            $("div.chartreport").append('<canvas id="scraperReport" height="120"></canvas>');
            loadDataDashboard(tglAwal, tglAkhir);
        });

        loadDataDashboard(tglAwal, tglAkhir);

        function loadDataDashboard(startDate, endDate) {
            $.ajax({
                url: "{{ url('fetch-data-dashboard') }}",
                type: "GET",
                data: {
                    start_date: startDate,
                    end_date: endDate
                },
                success: function(res) {
                    const labels = res.label;

                    const data = {
                        labels: labels,
                        datasets: [{
                            label: 'Hasil Profil Scraper',
                            backgroundColor: [
                                "#f44b13",
                                "#283fed",
                            ],
                            borderColor: 'rgb(255, 99, 132)',
                            barPercentage: 1,
                            barThickness: 80,
                            maxBarThickness: 100,
                            data: res.data,
                        }]
                    };

                    const config = {
                        type: 'bar',
                        data: data,
                        options: {}
                    };

                    const scraperReport = new Chart(
                        document.getElementById('scraperReport'),
                        config
                    );

                }
            })
        }
    </script>
@endpush
