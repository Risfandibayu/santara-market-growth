@extends('layouts.master')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Penerbit Coming Soon</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Penerbit Coming Soon
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Daftar Penerbit Coming Soon</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <a class="btn btn-primary mb-2" target="__blank" href="{{ url('penerbit/export-ig-coming-soon') }}">Export Akun Instagram</a>
                            <div class="table-responsive">
                                <table id="tableCommingSoon" class="no-style-no no-style">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>Perusahaan</th>
                                            <th>Brand</th>
                                            <th>HP</th>
                                            <th>Tanggal</th>
                                            <th>Dukung</th>
                                            <th>Suka</th>
                                            <th><i>Comment</i></th>
                                            <th>Kebutuhan Dana</th>
                                            <th><i>Share</i> Saham</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function filterTr() {
            const filter = $("#filter").val();
            $("#tableCommingSoon").DataTable().clear().destroy();
            loadData(filter);
        }

        loadData("");

        function loadData(filter) {
            var tableWithDraw = $("#tableCommingSoon").DataTable({
                ajax: '{{ url('/penerbit/fetch-coming-soon') }}' + "?filter=" + filter,
                responsive: true,
                processing: true,
                serverSide: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                order: [
                    [0, "asc"]
                ],
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "trader_name"
                    },
                    {
                        data: "company_name"
                    },
                    {
                        data: "trademark"
                    },
                    {
                        data: "phone"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "total_votes"
                    },
                    {
                        data: "total_likes"
                    },
                    {
                        data: "total_coments"
                    },
                    {
                        data: "capital_needs"
                    },
                    {
                        data: "investment"
                    },
                    {
                        data: "buttonAksi"
                    }
                ]
            });
        }
    </script>
@endpush
