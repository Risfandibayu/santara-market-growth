@extends('layouts.master')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Penerbit Coming Soon</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Penerbit Coming Soon
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Daftar Penerbit Coming Soon</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-primary mb-2" type="button" data-toggle="modal"
                                data-target="#modalImport">Import Facebook Profil</button>
                            <div class="table-responsive">
                                @if(Session::has('success'))
                                    <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert">
                                        <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong>Berhasil</strong> {{ Session::get('success') }}
                                    </div>
                                @endif
                                <table id="tableCommingSoon" class="no-style-no no-style">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Lengkap</th>
                                            <th>Lokasi</th>
                                            <th>Profil URL</th>
                                            <th>Profil</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 0; @endphp
                                        @foreach ($profiles as $item)
                                            @php $no++; @endphp
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $item->full_name }}</td>
                                                <td>{{ $item->location_name }}</td>
                                                <td><a href="{{ $item->profile_url }}" target="__blank">{{ $item->profile_url }}</a></td>
                                                <td><img src="{{ $item->profile_picture_url }}" width="100" /></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            {{-- Modal Import --}}
                            <div class="modal fade text-left" id="modalImport" tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Import Profil</h4>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" enctype="multipart/form-data"
                                                action="{{ url('penerbit/import_facebook_profil_follower/' . $penerbitId) }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label>File Profil</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="profilFile" class="custom-file-input"
                                                            id="customFile">
                                                        <label class="custom-file-label" for="customFile">Pilih
                                                            File</label>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-primary">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $("#tableCommingSoon").DataTable();
    </script>
@endpush
