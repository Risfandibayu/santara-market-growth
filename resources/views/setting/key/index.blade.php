@extends('layouts.master')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Engine Growthtool</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Engine Growthtool
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Atur Engine Growthtool</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            @if (Session::has('success'))
                                <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <strong>Berhasil</strong> {{ Session::get('success') }}
                                </div>
                            @endif
                            <div class="col-md-6">
                                <form method="POST" action="{{ url('store-phantombuster-key') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label><strong>X-Phantombuster-Key</strong></label>
                                        <div class="input-group mb-3">
                                            <input type="text" name="key" class="form-control"
                                                placeholder="X-Phantombuster-Key"
                                                value="{{ $phantomBuster != null ? $phantomBuster->key : '' }}" />
                                            <div class="input-group-append">
                                                <button type="submit" id="btnSubmit" class="btn btn-primary"><i
                                                        class="ft-save"></i>
                                                    Simpan</button>
                                                <button class="btn btn-primary hidden" id="btnLoading" type="button"
                                                    disabled>
                                                    <span class="spinner-border spinner-border-sm" role="status"
                                                        aria-hidden="true"></span>
                                                    Menyimpan...
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $("#btnSubmit").on("click", function() {
            $("#btnSubmit").addClass("hidden");
            $("#btnLoading").removeClass("hidden");
        });
    </script>
@endpush
