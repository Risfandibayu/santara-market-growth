 <!-- BEGIN: Main Menu-->

 <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
     <div class="main-menu-content">
         <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
             <li class="nav-item {{ '/' == request()->path() ? 'active' : '' }}"><a href="{{ url('/') }}"><i
                         class="la la-home"></i><span class="menu-title" data-i18n="eCommerce">Dashboard</span></a>
                 {{-- </li>
             <li class=" navigation-header"><span data-i18n="Admin Panels">Penerbit</span><i class="la la-ellipsis-h"
                     data-toggle="tooltip" data-placement="right" data-original-title="Penerbit"></i>
             <li class="nav-item {{ 'penerbit/coming-soon' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('/penerbit/coming-soon') }}"><i class="la la-briefcase"></i><span class="menu-title"
                         data-i18n="eCommerce">Coming
                         Soon</span></a>
             </li> --}}
             <li class="nav-item {{ 'calon-investor' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('calon-investor') }}"><i class="la la-briefcase"></i><span class="menu-title"
                         data-i18n="eCommerce">Calon Investor</span></a>
             </li>
             <li class=" navigation-header"><span data-i18n="Admin Panels">Data Scraper</span><i
                     class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right"
                     data-original-title="Data Scraper"></i>
             </li>
             <li class="nav-item {{ 'scraper-ig-phantombuster' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('scraper-ig-phantombuster') }}"><i class="icon-user-follow"></i><span
                         class="menu-title" data-i18n="eCommerce">Instagram Follower</span></a>
             </li>
             <li class=" nav-item {{ 'profil-scraper-ig-phantombuster' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('profil-scraper-ig-phantombuster') }}"><i class="icon-user-following"></i><span
                         class="menu-title" data-i18n="eCommerce">Instagram Profil</span></a>
             </li>
             <li class=" nav-item {{ 'result-scraper/instagram' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('result-scraper/instagram') }}"><i class="icon-user-following"></i><span
                         class="menu-title" data-i18n="eCommerce">Collecting Result</span></a>
             </li>
             {{-- <li class=" nav-item {{ 'profil-scraper-fb-phantombuster' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('profil-scraper-fb-phantombuster') }}"><i class="icon-user-following"></i><span
                         class="menu-title" data-i18n="eCommerce">Facebook Profil</span></a> --}}
             <li class=" navigation-header"><span data-i18n="Admin Panels">Pengaturan</span><i class="la la-ellipsis-h"
                     data-toggle="tooltip" data-placement="right" data-original-title="Pengaturan"></i>
             <li class=" nav-item {{ 'phantombuster-key' == request()->path() ? 'active' : '' }}"><a
                     href="{{ url('phantombuster-key') }}"><i class="icon-settings"></i><span class="menu-title"
                         data-i18n="eCommerce">Engine Growthtool</span></a>
             </li>
         </ul>
     </div>
 </div>

 <!-- END: Main Menu-->
