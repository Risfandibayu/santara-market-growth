<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', [App\Http\Controllers\DashboardController::class, 'index']);
    Route::get('/penerbit/coming-soon', [App\Http\Controllers\EmitenController::class, 'index'])->name('comming-soon');
    Route::get('/penerbit/facebook/{emitenId}', [App\Http\Controllers\EmitenController::class, 'showFacebook']);
    Route::get('/penerbit/instagram/{emitenId}', [App\Http\Controllers\EmitenController::class, 'showInstagram']);
    Route::post('/penerbit/import_instagram_profil_follower/{emitenId}', [App\Http\Controllers\EmitenController::class, 'importProfilInstagram']);
    Route::post('/penerbit/import_facebook_profil_follower/{emitenId}', [App\Http\Controllers\EmitenController::class, 'importProfilFacebook']);
    Route::get('/penerbit/fetch-coming-soon', [App\Http\Controllers\EmitenController::class, 'fetchData']);
    Route::get('/penerbit/export-ig-coming-soon', [App\Http\Controllers\EmitenController::class, 'exportIGComingSoon']);
    Route::get('/penerbit/export-ig-coming-soon/{emitenId}', [App\Http\Controllers\EmitenController::class, 'exportIGComingSoonPenerbit']);

    Route::get('/scraper-ig-phantombuster', [App\Http\Controllers\FollowerScraperController::class, 'indexInstagram']);
    Route::get('/profil-scraper-ig-phantombuster', [App\Http\Controllers\ProfilScraperController::class, 'indexInstagram']);
    Route::get('/profil-scraper-fb-phantombuster', [App\Http\Controllers\ProfilScraperController::class, 'indexFacebook']); 

    Route::post('/launch-scraper-ig-phantombuster/{containerID}', [App\Http\Controllers\ScraperController::class, 'launchIGScrapper']);
    Route::get('/fetch-container-phantombuster/{containerID}', [App\Http\Controllers\ScraperController::class, 'fetchContainerID']);
    Route::get('/result-container-phantombuster/{containerID}', [App\Http\Controllers\ScraperController::class, 'fetchObjectResult']);

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/phantombuster-key', [App\Http\Controllers\PhantombusterController::class, 'index']);
    Route::post('/store-phantombuster-key', [App\Http\Controllers\PhantombusterController::class, 'store']);

    Route::get('/fetch-data-dashboard', [App\Http\Controllers\DashboardController::class, 'getData']);

    Route::get('/penerbit/export-follower-ig/{emitenId}', [App\Http\Controllers\EmitenController::class, 'exportIGProfilScraper']);
    Route::get('/penerbit/fetch-follwer-ig/{emitenId}', [App\Http\Controllers\EmitenController::class, 'fetchDataInstagram']);
    Route::get('/penerbit/fetch-profil-follwer-ig/{emitenId}', [App\Http\Controllers\EmitenController::class, 'detailProfilIg']);
    
    Route::get('/result-scraper/instagram', [App\Http\Controllers\ResultScraperController::class, 'indexInstagram'])->name('result-scraper-ig');
    Route::get('/result-scraper/fetch-data-instagram', [App\Http\Controllers\ResultScraperController::class, 'fetchDataInstagram']);
    Route::post('/result-scraper/import-data-instagram', [App\Http\Controllers\ResultScraperController::class, 'importProfilInstagram']);
    Route::get('/result-scraper/export-data-instagram', [App\Http\Controllers\ResultScraperController::class, 'exportProfilInstagram']);

    Route::get('/calon-investor', [App\Http\Controllers\CalonInvestorController::class, 'index']);
    Route::get('/calon-investor/fetch-data', [App\Http\Controllers\CalonInvestorController::class, 'fetchData']);
    Route::post('/result-scraper/add-to-investor/{id}', [App\Http\Controllers\CalonInvestorController::class, 'addToInvestorList']);
    Route::post('/result-scraper/remove-to-investor/{id}', [App\Http\Controllers\CalonInvestorController::class, 'removeToInvestorList']);
    Route::get('/calon-investor/export-data', [App\Http\Controllers\CalonInvestorController::class, 'exportCalonInvestor']);

    Route::post('/result-scraper/add-to-all-investor', [App\Http\Controllers\CalonInvestorController::class, 'addToAllInvestorList']);
    
});

Auth::routes(['register' => false]);
