<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramFollowerScrapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_follower_scrapers', function (Blueprint $table) {
            $table->id();
            $table->string('profile_url')->nullable();
            $table->string('username')->nullable();
            $table->string('fullname')->nullable();
            $table->mediumText('image_url')->nullable();
            $table->bigInteger('instagram_id', false, false)->nullable();
            $table->tinyInteger('is_private')->nullable();
            $table->tinyInteger('is_verrified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_follower_scrapers');
    }
}
