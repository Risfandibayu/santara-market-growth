<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramScrapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_profil_scrapers', function (Blueprint $table) {
            $table->id();
            $table->string('url_profil')->nullable();
            $table->string('profil_name')->nullable();
            $table->string('full_name')->nullable();
            $table->mediumText('bio')->nullable();
            $table->bigInteger('follower', false, false)->nullable();
            $table->bigInteger('following', false, false)->nullable();
            $table->bigInteger('instagram_id', false, false)->nullable();
            $table->tinyInteger('is_business_account')->nullable();
            $table->string('business_category', 100)->nullable();
            $table->bigInteger('phone_number',  false, false)->nullable();
            $table->tinyInteger('is_private')->nullable();
            $table->tinyInteger('is_verrified')->nullable();
            $table->mediumText('image_url')->nullable();
            $table->tinyInteger('is_insvestor_active')->default(0);
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_scrapers');
    }
}
