<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacebookProfilScrapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_profil_scrapers', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->nullable();
            $table->string('location_name')->nullable();
            $table->string('location_type')->nullable();
            $table->string('location_url')->nullable();
            $table->string('facebook', 100)->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('interested_in', 50)->nullable();
            $table->string('languages')->nullable();
            $table->mediumText('bio')->nullable();
            $table->mediumText('profile_picture_url')->nullable();
            $table->string('facebook_id', 100)->nullable();
            $table->string('profile_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_profil_scrapers');
    }
}
