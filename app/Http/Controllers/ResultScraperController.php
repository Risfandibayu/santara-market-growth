<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InstagramProfilScraper;
use App\Imports\ResultScraperNoPenerbit;
use App\Exports\ResultScraperExport;
use App\Models\CalonInvestor;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use DB;

class ResultScraperController extends Controller
{

    public function indexInstagram()
    {
        return view('result-scraper.instagram');
    }
    
    public function fetchDataInstagram(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $queryRecord = InstagramProfilScraper::query();
        $queryRecord->where('is_insvestor_active', 0);

        $queryRecordwithFilter = InstagramProfilScraper::query();
        $queryRecordwithFilter->where('is_insvestor_active', 0);

        $query = InstagramProfilScraper::query();
        $query->skip($start)
            ->take($rowperpage)
            ->where('is_insvestor_active', 0);

        if($startDate != "" && $endDate != ""){
            $queryRecord->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
            $queryRecordwithFilter->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
            $query->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
        }

        if($searchValue != ""){
            $queryRecord->where('email', 'like', '%'.$searchValue.'%')
                ->orWhere('full_name', 'like', '%'.$searchValue.'%');
            $queryRecordwithFilter->where('email', 'like', '%'.$searchValue.'%')
                ->orWhere('full_name', 'like', '%'.$searchValue.'%');
            $query->where('email', 'like', '%'.$searchValue.'%')
                ->orWhere('full_name', 'like', '%'.$searchValue.'%');
        }

        if($columnName == "id"){
            $query->orderBy("created_at", "DESC");
        }else{
            $query->orderBy($columnName, $columnSortOrder);
        }

        $totalRecords = $queryRecord->count();
        $totalRecordswithFilter = $queryRecordwithFilter->count();
        $profiles = $query->get();

        $data = [];
        foreach ($profiles as $row) { 
            if($row->is_insvestor_active == 0){
                $button = '<label><input onclick="addInvestor('.$row->id.')" type="checkbox" /> Add to Investor List</label>';
            }else{
                $button = '<label><input onclick="removeInvestor('.$row->id.')" checked type="checkbox" /> Remove to Investor List</label>';
            }

            array_push($data, [
                'id' => $row->id,
                'full_name' => $row->full_name,
                'email' => $row->email,
                'phone_number' => $row->phone_number,
                'scrapered_at' => $row->scrapered_at != null ? tgl_indo(Carbon::parse($row->scrapered_at)->format('Y-m-d')) : "-",
                'created_at' => tgl_indo(Carbon::parse($row->created_at)->format('Y-m-d')),
                'btnAksi' => $button
            ]);
        } 

        $response = array(
            "tglAwal" => $startDate,
            "tglAkhir" => $endDate,
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function importProfilInstagram(Request $request)
    {
        Excel::import(new ResultScraperNoPenerbit(), $request->profilFile);
        return redirect()->back()->with('success', 'Profil User Instagram Berhasil diimport!');
    }

    public function exportProfilInstagram(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        return Excel::download(new ResultScraperExport($startDate, $endDate), 'Result-Scraper-Instagram.xlsx');
    }

}
