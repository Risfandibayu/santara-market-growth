<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CalonPenerbit;
use App\Models\CalonInvestor;
use App\Models\Emiten;
use DB;

class CalonPenerbitController extends Controller
{
    
    public function addToCalonPenerbit($investorId)
    {
        DB::beginTransaction();
        try{

            $calonPenerbit = new CalonPenerbit();
            $calonPenerbit->calon_investor_id = $investorId;
            $calonPenerbit->is_active = 1;
            $calonPenerbit->is_deleted = 0;
            $calonPenerbit->save();

            DB::commit();

            return response()->json(["code" => 200, "message" => "Berhasil menambahkan ke calon penerbit", "status" => true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["code" => 200, "message" => "Gagal menambahkan ke calon penerbit", "status" => false]);
        }
    }

}
