<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Emiten;
use App\Models\InstagramProfilScraper;
use App\Models\FacebookProfilScraper;
use App\Exports\PenerbitComingSoonExport;
use App\Imports\ProfilInstagramImport;
use App\Imports\ProfilFacebookImport;
use App\Exports\ProfilScraperExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class EmitenController extends Controller
{
    
    public function index()
    {
        return view('emiten.coming-soon.index');
    }

    public function exportIGComingSoon()
    {
        return Excel::download(new PenerbitComingSoonExport(null), 'coming-soon-instagram.csv');
    }

    public function exportIGComingSoonPenerbit($penerbitId)
    {
        return Excel::download(new PenerbitComingSoonExport($penerbitId), 'coming-soon-instagram-'.$penerbitId.'.csv');
    }

    public function fetchData(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $totalRecords = $this->getCount($filter);
        $totalRecordswithFilter = $this->getCountFilter($searchValue, $filter);
        $pralisting = $this->getDataAPIPralisting($searchValue, $start, $rowperpage, $filter);

        $data = [];
        foreach($pralisting as $row){
            $status = \DB::connection('mysql2')->table('emiten_status_histories')->where('emiten_id', $row->id)
                ->select('status')->orderBy('id', 'DESC')->limit(1)
                ->first();

            $investment = \DB::connection('mysql2')->table('emiten_pre_investment_plans')->where('emiten_id', $row->id)
                ->where('is_deleted', 0)
                ->select(\DB::raw('COALESCE(SUM(amount),0) as investment'))
                ->first();

            $total_likes = \DB::connection('mysql2')->table('emiten_votes')->where('likes', 1)
                ->where('emiten_id', $row->id)
                ->where('is_deleted', 0)
                ->select(\DB::raw('COUNT(likes) as total_likes'))
                ->first();

            $total_votes = \DB::connection('mysql2')->table('emiten_votes')->where('vote', 1)
                ->where('emiten_id', $row->id)
                ->where('is_deleted', 0)
                ->select(\DB::raw('COUNT(vote) as total_votes'))
                ->first();

            $statusVerifikasi = "";
            if($row->is_verified_bisnis == 1){
                $statusVerifikasi = "<span class='badge badge-success'>Terverifikasi</span>";
            }else if($row->is_verified_bisnis == 2){
                $statusVerifikasi = "<span class='badge badge-danger'>Ditolak</span>";
            }else if($row->is_verified_bisnis == null){
                $statusVerifikasi = "-";
            }

            $totalComents = \DB::connection('mysql2')->select('(SELECT COALESCE(COUNT(comment), 0) + COALESCE(COUNT(ch.comment_histories), 0) as total_coments from emiten_comments left join (select emiten_comment_id, COUNT(comment) as comment_histories from emiten_comment_histories where is_deleted = 0 group by id) as ch on emiten_comments.id = ch.emiten_comment_id where emiten_comments.emiten_id = '.$row->id.' and emiten_comments.is_deleted = 0)');

            $button = ' <a href="'.url('penerbit/export-ig-coming-soon/'.$row->id).'" class="btn btn-sm btn-primary"><span class="icon-printer"></span> Instagram</a>';
            $button .= ' <a href="'.url('penerbit/instagram/'.$row->id).'" class="btn btn-sm btn-warning"><span class="icon-eye"></span> Follower</a>';
            //$button .= ' <a href="'.url('penerbit/facebook/'.$row->id).'" class="btn btn-sm btn-primary">Facebook</a>';

            array_push($data, [
                "id" => $row->id,
                'company_name' => $row->company_name,
                'trademark' => $row->trademark,
                'capital_needs' => rupiah($row->avg_capital_needs),
                'is_verified' => $row->is_verified,
                'created_at' => tgl_indo(date('Y-m-d', strtotime($row->created_at))),
                'trader_name' => $row->name,
                'phone' => $row->phone,
                "status" => $statusVerifikasi,
                'investment' => intval($row->avg_general_share_amount).' %',
                'total_likes' => $total_likes->total_likes,
                'total_votes' => $total_votes->total_votes,
                'total_coments' => $totalComents[0]->total_coments,
                'buttonAksi' => $button
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function getCount($filter)
    {
        if($filter != ""){
            $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                    'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                    'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                    't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader')
                ->where('emitens.is_verified_bisnis', $filter)
                ->where('emitens.is_deleted',0)
                ->where('emitens.is_verified',1)
                ->where('emitens.is_pralisting',1)
                ->where('emitens.is_coming_soon',1)
                ->groupBy('emitens.id')
                ->get();
            return count($pralisting);
        }else{
            $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                    ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                    ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                    ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                    ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                        'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                        'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                        't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader')
                    ->where('emitens.is_deleted',0)
                    ->where('emitens.is_verified',1)
                    ->where('emitens.is_pralisting',1)
                    ->where('emitens.is_coming_soon',1)
                    ->groupBy('emitens.id')
                    ->get();
            return count($pralisting);
        }
    }

    public function getCountFilter($searchValue, $filter)
    {
        if($filter != ""){
            $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                    ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                    ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                    ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                    ->where('emitens.is_verified_bisnis', $filter)
                    ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                    ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                        'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                        'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                        't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader')
                    ->where('emitens.is_deleted',0)
                    ->where('emitens.is_verified',1)
                    ->where('emitens.is_pralisting',1)
                    ->where('emitens.is_coming_soon',1)
                    ->groupBy('emitens.id')
                    ->get();
            return count($pralisting);
        }else{
            $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                    ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                    ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                    ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                    ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                    ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                        'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                        'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                        't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader')
                    ->where('emitens.is_deleted',0)
                    ->where('emitens.is_verified',1)
                    ->where('emitens.is_pralisting',1)
                    ->where('emitens.is_coming_soon',1)
                    ->groupBy('emitens.id')
                    ->get();
            return count($pralisting);
        }
    }


    public function getDataAPIPralisting($searchValue, $start, $rowperpage, $filter)
    {
        if($filter != ""){
            $pralisting = Emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                    'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                    'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                    't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader',
                    'emitens.avg_general_share_amount', 'emitens.avg_capital_needs')
                ->where('emitens.is_deleted',0)
                ->where('emitens.is_verified_bisnis', $filter)
                ->where('emitens.is_verified',1)
                ->where('emitens.is_pralisting',1)
                ->where('emitens.is_coming_soon',1)
                ->groupBy('emitens.id')
                ->orderby('emitens.created_at','DESC')
                ->skip($start)
                ->take($rowperpage)
                ->get();
            return $pralisting;
        }else{
            $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                    ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                    ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                    ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                    ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                    ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                        'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                        'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                        't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader',
                        'emitens.avg_general_share_amount', 'emitens.avg_capital_needs')
                    ->where('emitens.is_deleted',0)
                    ->where('emitens.is_verified',1)
                    ->where('emitens.is_pralisting',1)
                    ->where('emitens.is_coming_soon',1)
                    ->groupBy('emitens.id')
                    ->orderby('emitens.created_at','DESC')
                    ->skip($start)
                    ->take($rowperpage)
                    ->get();
            return $pralisting;
        }
    }

    public function showInstagram($penerbitId)
    {
        $profilFollowers = InstagramProfilScraper::where('emiten_id', $penerbitId)->get();
        return view('emiten.coming-soon.instagram', compact('profilFollowers', 'penerbitId'));
    }

    public function fetchDataInstagram(Request $request, $penerbitId)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $queryRecord = InstagramProfilScraper::query();
        $queryRecord->where('emiten_id', $penerbitId);

        $queryRecordwithFilter = InstagramProfilScraper::query();
        $queryRecordwithFilter->where('emiten_id', $penerbitId);

        $query = InstagramProfilScraper::query();
        $query->where('emiten_id', $penerbitId)
            ->skip($start)
            ->take($rowperpage);

        if($startDate != "" && $endDate != ""){
            $queryRecord->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
            $queryRecordwithFilter->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
            $query->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
        }

        $totalRecords = $queryRecord->count();
        $totalRecordswithFilter = $queryRecordwithFilter->count();
        $profiles = $query->get();

        $data = [];
        foreach ($profiles as $row) { 
            $button = '<button onclick="detail('.$row->id.')" type="button" class="btn btn-primary btn-sm">Detail</button>';
            array_push($data, [
                'id' => $row->id,
                'full_name' => $row->full_name,
                'email' => $row->email,
                'phone_number' => $row->phone_number,
                'scrapered_at' => $row->scrapered_at != null ? tgl_indo(Carbon::parse($row->scrapered_at)->format('Y-m-d')) : "-",
                'created_at' => tgl_indo(Carbon::parse($row->created_at)->format('Y-m-d')),
                'btnAksi' => $button
            ]);
        } 

        $response = array(
            "tglAwal" => $startDate,
            "tglAkhir" => $endDate,
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function detailProfilIg($id)
    {
        $profil = InstagramProfilScraper::find($id);
        return response()->json(["data" => $profil]);
    }

    public function showFacebook($penerbitId)
    {
        $profiles = FacebookProfilScraper::where('emiten_id', $penerbitId)->get();
        return view('emiten.coming-soon.facebook', compact('profiles', 'penerbitId'));
    }

    public function importProfilInstagram(Request $request, $penerbitId)
    {
        Excel::import(new ProfilInstagramImport($penerbitId), $request->profilFile);
        return redirect()->back()->with('success', 'Profil User Intagram Berhasil diimport!');
    }

    public function importProfilFacebook(Request $request, $penerbitId)
    {
        Excel::import(new ProfilFacebookImport($penerbitId), $request->profilFile);
        return redirect()->back()->with('success', 'Profil User Facebook Berhasil diimport!');
    }

    public function exportIGProfilScraper($penerbitId)
    {
        $emiten = Emiten::select('trademark')->where('id', $penerbitId)->first();
        return Excel::download(new ProfilScraperExport($penerbitId), $emiten->trademark.'-Follower-'.$penerbitId.'.csv');
    }


}
