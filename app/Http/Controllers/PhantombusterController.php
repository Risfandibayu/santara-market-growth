<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhantombusterKey;

class PhantombusterController extends Controller
{
    
    public function index()
    {
        $phantomBuster = PhantombusterKey::first();
        return view('setting.key.index', compact('phantomBuster'));
    }

    public function store(Request $request)
    {
        $cekphantomBuster = PhantombusterKey::count();
        if($cekphantomBuster > 0){
            $phantomBuster = PhantombusterKey::first();
            $phantomBuster->key = $request->key;
        }else{
            $phantomBuster = new PhantombusterKey();
            $phantomBuster->key = $request->key;
        }
        $phantomBuster->save();
        return redirect('phantombuster-key')->with('success', 'Berhasil mengatur phantombuster key');
    }

}
