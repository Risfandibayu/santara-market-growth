<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhantombusterKey;

class ScraperController extends Controller
{
    
    public function launchIGScrapper($containerID)
    {
        $phantomBuster = PhantombusterKey::first();
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', config('global.URL_PHANTOMBUSTER').'/agents/launch', [
                'body' => '{"id": "'.$containerID.'" }',
                'headers' => [
                    'Content-Type' => 'application/json',
                    'X-Phantombuster-Key' => $phantomBuster->key,
                ],
            ]);
            $data = json_decode($response->getBody(), TRUE);
        }catch (\GuzzleHttp\Exception\RequestException $e) {
            $data = $e->getMessage();
        }
      
        return response()->json(["code" => 200, "data" => $data]);
    }

    public function fetchContainerID($containerID)
    {
        $phantomBuster = PhantombusterKey::first();
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('GET', config('global.URL_PHANTOMBUSTER').'/containers/fetch?id='.$containerID, [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Phantombuster-Key' => $phantomBuster->key,
                ],
            ]);
            $data = json_decode($response->getBody(), TRUE);
        }catch (\GuzzleHttp\Exception\RequestException $e) {
            $data = $e->getMessage();
        }

        return response()->json(["code" => 200, "data" => $data]);
    }

    public function fetchObjectResult($containerID)
    {
        $phantomBuster = PhantombusterKey::first();
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('GET', config('global.URL_PHANTOMBUSTER').'/containers/fetch-result-object?id='.$containerID, [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Phantombuster-Key' => $phantomBuster->key,
                ],
            ]);
            $data = json_decode($response->getBody(), TRUE);
        }catch (\GuzzleHttp\Exception\RequestException $e) {
            $data = $e->getMessage();
        }
        return response()->json(["code" => 200, "data" => $data]);
    }


}
