<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhantombusterKey;

class FollowerScraperController extends Controller
{
    
    public function indexInstagram()
    {
        $phantomBuster = PhantombusterKey::first();
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', config('global.URL_PHANTOMBUSTER').'/agents/fetch-all', [
            'headers' => [
              'Accept' => 'application/json',
              'X-Phantombuster-Key' => $phantomBuster->key,
            ],
        ]);

        $ig = json_decode($response->getBody(), TRUE);
        $data = [];
        foreach ($ig as $row) {
            if($row['name'] == 'Instagram Follower Collector'){
                $fetchData = new \stdClass; 
                $fetchData->id = $row['id'];
                $fetchData->name = $row['name'];
                array_push($data, $fetchData);
            }
        }
        return view('follower-scraper.instagram.index', compact('data'));
    }

}
