<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InstagramProfilScraper;
use App\Models\FacebookProfilScraper;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    
    public function index()
    {
        return view('dashboard.index');
    }

    public function getData(Request $request)
    {
        $instagrams = InstagramProfilScraper::query();
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        if($startDate != "" && $endDate != ""){
            $instagrams->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
        }
        $instagramData = $instagrams->count();

        $facebooks = FacebookProfilScraper::query();
        if($startDate != "" && $endDate != ""){
            $facebooks->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate);
        }
        $facebookData = $facebooks->count();
        // $instagrams = InstagramProfilScraper::whereBetween('created_at', [
        //     $now->startOfWeek()->format('Y-m-d'),
        //     $now->endOfWeek()->format('Y-m-d')
        // ])->get();
        $data = [];
        $data['label'][] = "Instagram";
        // $data['label'][] = "Facebook";
        $data['data'][] = $instagramData;
        // $data['data'][] = $facebookData;
        return response()->json($data);
    }

}
