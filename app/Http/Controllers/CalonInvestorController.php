<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CalonInvestor;
use App\Models\InstagramProfilScraper;
use App\Models\UserSantara;
use App\Models\Trader;
use App\Models\BalanceUtama;
use Carbon\Carbon;
use App\Exports\CalonInvestorExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class CalonInvestorController extends Controller
{

    public function index()
    {
        return view('calon-investor.index');
    }
    
    public function fetchData(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; // tiper sorting asc / desc
        $searchValue = $search_arr['value'];
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $queryRecord = CalonInvestor::query();
        $queryRecord->join('instagram_profil_scrapers as ig', 'ig.id', '=', 'calon_investors.ig_profil_id')
            ->where('calon_investors.is_deleted', 0)
            ->where('calon_investors.is_active', 1)
            ->where('ig.is_insvestor_active', 1)
            ->where('calon_investors.is_emiten_active', 0);

        $queryRecordwithFilter = CalonInvestor::query();
        $queryRecordwithFilter->join('instagram_profil_scrapers as ig', 'ig.id', '=', 'calon_investors.ig_profil_id')
            ->where('calon_investors.is_deleted', 0)
            ->where('calon_investors.is_active', 1)
            ->where('ig.is_insvestor_active', 1)
            ->where('calon_investors.is_emiten_active', 0);

        $query = CalonInvestor::query();
        $query->join('instagram_profil_scrapers as ig', 'ig.id', '=', 'calon_investors.ig_profil_id')
            ->where('calon_investors.is_deleted', 0)
            ->where('calon_investors.is_active', 1)
            ->select('calon_investors.id', 'ig.full_name', 'ig.email', 'ig.phone_number', 'ig.scrapered_at', 'calon_investors.created_at',
                'calon_investors.password_user_santara')
            ->skip($start)
            ->take($rowperpage)
            ->where('ig.is_insvestor_active', 1)
            ->where('calon_investors.is_emiten_active', 0);

        if($searchValue != ""){
            $queryRecord->where('ig.email', 'like', '%'.$searchValue.'%')
                ->orWhere('ig.full_name', 'like', '%'.$searchValue.'%');
            $queryRecordwithFilter->where('ig.email', 'like', '%'.$searchValue.'%')
                ->orWhere('ig.full_name', 'like', '%'.$searchValue.'%');
            $query->where('ig.email', 'like', '%'.$searchValue.'%')
                ->orWhere('ig.full_name', 'like', '%'.$searchValue.'%');
        }

        if($startDate != "" && $endDate != ""){
            $queryRecord->whereDate('calon_investors.created_at', '>=', $startDate)
                ->whereDate('calon_investors.created_at', '<=', $endDate);
            $queryRecordwithFilter->whereDate('calon_investors.created_at', '>=', $startDate)
                ->whereDate('calon_investors.created_at', '<=', $endDate);
            $query->whereDate('calon_investors.created_at', '>=', $startDate)
                ->whereDate('calon_investors.created_at', '<=', $endDate);
        }

        if($columnName == "id"){
            $query->orderBy("calon_investors.created_at", "DESC");
        }else{
            $query->orderBy($columnName, $columnSortOrder);
        }

        $totalRecords = $queryRecord->count();
        $totalRecordswithFilter = $queryRecordwithFilter->count();
        $profiles = $query->get();

        $data = [];
        foreach ($profiles as $row) { 
            if($row->is_insvestor_active == 0){
                $button = '<label><input onclick="addCommingSoon('.$row->id.')" type="checkbox" /> Add to Comming Soon List</label>';
            }
            // else{
            //     $button = '<label><input onclick="removeInvestor('.$row->id.')" checked type="checkbox" /> Remove to Investor List</label>';
            // }

            array_push($data, [
                'id' => $row->id,
                'full_name' => $row->full_name,
                'email' => $row->email,
                'password_user_santara' => $row->password_user_santara,
                'phone_number' => $row->phone_number,
                'scrapered_at' => $row->scrapered_at != null ? tgl_indo(Carbon::parse($row->scrapered_at)->format('Y-m-d')) : "-",
                'created_at' => tgl_indo(Carbon::parse($row->created_at)->format('Y-m-d')),
            ]);
        } 

        $response = array(
            "order" => $columnName,
            "tglAwal" => $startDate,
            "tglAkhir" => $endDate,
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function addToInvestorList($ig_profil_id)
    {
        DB::beginTransaction();
        try{

            $getInfoProfil =  InstagramProfilScraper::where('id', $ig_profil_id)
                ->where('is_deleted', 0)
                ->select('full_name', 'email', 'phone_number')
                ->first();

            $passwordUser = \Str::random(8);

            $userCek = UserSantara::where('email', $getInfoProfil->email)->first();
            if($userCek == null){
                $user = new UserSantara();
                $user->uuid = (string) \Str::uuid();
                $user->email = $getInfoProfil->email;
                $user->password = \Hash::make($passwordUser);
                $user->role_id = 2;
                $user->is_verified = 1;
                $user->two_factor_auth = 0;
                $user->is_deleted = 0;
                $user->is_otp = 0;
                $user->attempt = 0;
                $user->attempt_email = 0;
                if (config('global.STATUS_DEV')){
                    $user->privacy = 0;
                }
                $user->save();

                $trader = new Trader();
                $trader->uuid = (string) \Str::uuid();
                $trader->user_id = $user->id;
                $trader->name = $getInfoProfil->full_name;
                $trader->phone = $getInfoProfil->phone_number;
                $trader->save();
    
                $saldo = new BalanceUtama();
                $saldo->trader_id = $trader->id;
                $saldo->balance = 0;
                $saldo->save();
            }

            $calonInvestor = new CalonInvestor();
            $calonInvestor->ig_profil_id = $ig_profil_id;
            $calonInvestor->is_active = 1;
            $calonInvestor->is_deleted = 0;
            $calonInvestor->password_user_santara = $passwordUser;
            $calonInvestor->user_id_santara = $userCek != null ? $userCek->id : $user->id;
            $calonInvestor->save();

            InstagramProfilScraper::where('id', $ig_profil_id)
                ->where('is_deleted', 0)
                ->update([
                    'is_insvestor_active' => 1
                ]);
           
            DB::commit();

            return response()->json(["code" => 200, "message" => "Berhasil menambahkan ke calon investor", "status" => true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["code" => 200, "message" => "Gagal menambahkan ke calon investor", "status" => false]);
        }
       
    }

    public function addToAllInvestorList()
    {
        ini_set('memory_limit', '-1');

        DB::beginTransaction();
        try{

            InstagramProfilScraper::where('is_deleted', 0)
                ->where('is_insvestor_active', 0)
                ->select('full_name', 'email', 'phone_number', 'id')
                ->chunk(1000, function ($igs){

                foreach ($igs as $row) {
                    
                    $passwordUser = \Str::random(8);

                    $userCek = UserSantara::where('email', $row->email)->first();
                    if($userCek == null){
                        $user = new UserSantara();
                        $user->uuid = (string) \Str::uuid();
                        $user->email = $row->email;
                        $user->password = \Hash::make($passwordUser);
                        $user->role_id = 2;
                        $user->is_verified = 1;
                        $user->two_factor_auth = 0;
                        $user->is_deleted = 0;
                        $user->is_otp = 0;
                        $user->attempt = 0;
                        $user->attempt_email = 0;
                        if (config('global.STATUS_DEV')){
                            $user->privacy = 0;
                        }
                        $user->save();
                  
                        $trader = new Trader();
                        $trader->uuid = (string) \Str::uuid();
                        $trader->user_id = $user->id;
                        $trader->name = removeEmoji($row->full_name);
                        $trader->phone = $row->phone_number;
                        $trader->save();
                  
                        $saldo = new BalanceUtama();
                        $saldo->trader_id = $trader->id;
                        $saldo->balance = 0;
                        $saldo->save();
                    }
        
                    $calonInvestor = new CalonInvestor();
                    $calonInvestor->ig_profil_id = $row->id;
                    $calonInvestor->is_active = 1;
                    $calonInvestor->is_deleted = 0;
                    $calonInvestor->password_user_santara = $userCek != null ? null : $passwordUser;
                    $calonInvestor->user_id_santara = $userCek != null ? $userCek->id : $user->id;
                    $calonInvestor->save();
        
                    InstagramProfilScraper::where('id', $row->id)
                        ->where('is_deleted', 0)
                        ->update([
                            'is_insvestor_active' => 1
                        ]);

                }
           });

           DB::commit();

           return response()->json(["code" => 200, "message" => "Berhasil menambahkan ke calon investor", "status" => true]);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["code" => 200, "message" => "Gagal menjadikan data ke calon investor", "status" => false]);
        }
    }

    public function removeToInvestorList($id)
    {
        DB::beginTransaction();
        try{

            $calonInvestor = CalonInvestor::find($id);
            $calonInvestor->is_active = 0;
            $calonInvestor->save();

            $igProfil = InstagramProfilScraper::find($calonInvestor->ig_profil_id);
            $igProfil->is_insvestor_active = 0;
            $igProfil->save();

            DB::commit();

            return response()->json(["code" => 200, "message" => "Berhasil menghapus dari calon investor", "status" => true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["code" => 200, "message" => "Gagal menghapus dari calon investor", "status" => false]);
        }
    }

    public function exportCalonInvestor(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        return Excel::download(new CalonInvestorExport($startDate, $endDate), 'Calon-Investor.xlsx');
    }

}
