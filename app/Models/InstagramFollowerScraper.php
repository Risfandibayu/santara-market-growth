<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstagramFollowerScraper extends Model
{
    use HasFactory;
    protected $table = "instagram_follower_scrapers";
}
