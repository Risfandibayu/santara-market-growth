<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacebookProfilScraper extends Model
{
    use HasFactory;
    protected $table = "facebook_profil_scrapers";
    protected $fillable = [
        'full_name',
        'location_name',
        'location_type',
        'location_url',
        'facebook',
        'gender',
        'interested_in',
        'languages',
        'bio',
        'profile_picture_url',
        'facebook_id',
        'profile_url',
        'emiten_id'
    ];
}
