<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalonInvestor extends Model
{
    use HasFactory;
    protected $table = "calon_investors";
}
