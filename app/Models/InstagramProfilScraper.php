<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstagramProfilScraper extends Model
{
    use HasFactory;
    protected $table = "instagram_profil_scrapers";
    protected $fillable = [
        'url_profil',
        'profil_name',
        'full_name',
        'bio',
        'follower',
        'following',
        'instagram_id',
        'is_business_account',
        'phone_number',
        'is_private',
        'is_verrified',
        'image_url',
        'email',
        'website',
        'scrapered_at'
    ];
}