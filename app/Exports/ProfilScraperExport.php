<?php

namespace App\Exports;

use App\Models\InstagramProfilScraper;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;

class ProfilScraperExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $penerbitId;

    public function __construct($penerbitId) {
        $this->penerbitId = $penerbitId;
    }

    public function collection()
    {
        $profilFollowers = InstagramProfilScraper::where('emiten_id', $this->penerbitId)
            ->select('full_name', 'email', 'phone_number', 'scrapered_at', 'created_at')
            ->get();
        $data = [];
        foreach ($profilFollowers as $row) {
            array_push($data, [
                'full_name' => $row->full_name,
                'email' => $row->email,
                'phone_number' => $row->phone_number,
                'scrapered_at' => $row->scrapered_at != null ? tgl_indo(Carbon::parse($row->scrapered_at)->format('Y-m-d')) : "-",
                'created_at' => tgl_indo(Carbon::parse($row->created_at)->format('Y-m-d'))
            ]);
        }
        return collect($data);
    }

    public function headings(): array
    {
        return [
            'Fullname',
            'Email',
            'Phone',
            'Scrapered At',
            'Created At'
        ];
    }
}
