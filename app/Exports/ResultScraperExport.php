<?php

namespace App\Exports;

use App\Models\InstagramProfilScraper;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;

class ResultScraperExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $startDate;
    protected $endDate;

    public function __construct($startDate, $endDate) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function collection()
    {
        if($this->startDate != "" && $this->endDate != ""){
            $profilFollowers = InstagramProfilScraper::select('full_name', 'email', 'phone_number', 'scrapered_at', 'created_at')
                ->where('is_insvestor_active', 0)
                ->whereDate('created_at', '>=', $this->startDate)
                ->whereDate('created_at', '<=', $this->endDate)
                ->get();
        }else{
            $profilFollowers = InstagramProfilScraper::select('full_name', 'email', 'phone_number', 'scrapered_at', 'created_at')
                ->where('is_insvestor_active', 0)
                ->get();
        }
        $data = [];
        $no = 0;
        foreach ($profilFollowers as $row) {
            $no++;
            array_push($data, [
                'no' => $no,
                'full_name' => $row->full_name,
                'email' => $row->email,
                'phone_number' => $row->phone_number,
                'scrapered_at' => $row->scrapered_at != null ? tgl_indo(Carbon::parse($row->scrapered_at)->format('Y-m-d')) : "-",
                'created_at' => tgl_indo(Carbon::parse($row->created_at)->format('Y-m-d'))
            ]);
        }
        return collect($data);
    }

    public function headings(): array
    {
        return [
            'No',
            'Fullname',
            'Email',
            'Phone',
            'Scrapered At',
            'Created At'
        ];
    }
}
