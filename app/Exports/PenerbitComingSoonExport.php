<?php

namespace App\Exports;

use App\Models\Emiten;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PenerbitComingSoonExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $penerbitId;

    public function __construct($penerbitId) {
        $this->penerbitId = $penerbitId;
    }

    public function collection()
    {
        if($this->penerbitId != null){
            $emiten = Emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                ->select('emitens.company_name', 'emitens.instagram', 'emitens.facebook')
                ->where('emitens.is_deleted',0)
                ->where('emitens.is_verified',1)
                ->where('emitens.is_pralisting',1)
                ->where('emitens.is_coming_soon',1)
                ->where('emitens.id', $this->penerbitId)
                ->get();
        }else{
            $emiten = Emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                ->select('emitens.company_name', 'emitens.instagram', 'emitens.facebook')
                ->where('emitens.is_deleted',0)
                ->where('emitens.is_verified',1)
                ->where('emitens.is_pralisting',1)
                ->where('emitens.is_coming_soon',1)
                ->groupBy('emitens.id')
                ->get();
        }
        $data = [];
        foreach($emiten as $row){
            $ig_url = str_replace("@", "", $row->instagram);
            if(str_contains($ig_url, 'https://instagram.com/')){
                $ig_url = $ig_url;
            }elseif(str_contains($ig_url, 'https://www.instagram.com/')){
                $ig_url = $ig_url;
            }else{
                $ig_url = 'https://instagram.com/'.$ig_url;
            }
            array_push($data, [
                'company_name' => $row->company_name,
                'instagram' => $ig_url,
                // 'facebook' => $row->facebook
            ]);
        }
        return collect($data);
    }

    public function headings(): array
    {
        return [
            'Name',
            'instagramUrl',
            // 'facebookUrl'
        ];
    }
}
