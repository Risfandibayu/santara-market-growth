<?php

namespace App\Exports;

use App\Models\CalonInvestor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;

class CalonInvestorExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $startDate;
    protected $endDate;

    public function __construct($startDate, $endDate) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function collection()
    {
        if($this->startDate != "" && $this->endDate != ""){
            $calonInvestors = CalonInvestor::select('calon_investors.id', 'ig.full_name', 'ig.email', 'ig.phone_number', 
                    'ig.scrapered_at', 'calon_investors.created_at',
                    'calon_investors.password_user_santara')
                ->join('instagram_profil_scrapers as ig', 'ig.id', '=', 'calon_investors.ig_profil_id')
                ->where('calon_investors.is_deleted', 0)
                ->where('calon_investors.is_active', 1)
                ->where('ig.is_insvestor_active', 1)
                ->where('calon_investors.is_emiten_active', 0)
                ->whereDate('calon_investors.created_at', '>=', $this->startDate)
                ->whereDate('calon_investors.created_at', '<=', $this->endDate)
                ->get();
        }else{
            $calonInvestors = CalonInvestor::select('calon_investors.id', 'ig.full_name', 'ig.email', 'ig.phone_number', 
                    'ig.scrapered_at', 'calon_investors.created_at',
                    'calon_investors.password_user_santara')
                ->join('instagram_profil_scrapers as ig', 'ig.id', '=', 'calon_investors.ig_profil_id')
                ->where('calon_investors.is_deleted', 0)
                ->where('calon_investors.is_active', 1)
                ->where('ig.is_insvestor_active', 1)
                ->where('calon_investors.is_emiten_active', 0)
                ->get();
        }
        $data = [];
        $no = 0;
        foreach ($calonInvestors as $row) {
            $no++;
            array_push($data, [
                'no' => $no,
                'full_name' => $row->full_name,
                'email' => $row->email,
                'password_user_santara' => $row->password_user_santara,
                'phone_number' => $row->phone_number,
                'scrapered_at' => $row->scrapered_at != null ? tgl_indo(Carbon::parse($row->scrapered_at)->format('Y-m-d')) : "-",
                'created_at' => tgl_indo(Carbon::parse($row->created_at)->format('Y-m-d'))
            ]);
        }
        return collect($data);
    }

    public function headings(): array
    {
        return [
            'No',
            'Fullname',
            'Email',
            'Password User',
            'Phone',
            'Scrapered At',
            'Created At'
        ];
    }

}
