<?php

namespace App\Imports;

use App\Models\InstagramProfilScraper;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ProfilInstagramImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    protected $penerbitId;

    public function __construct($penerbitId) {
        $this->penerbitId = $penerbitId;
    }


    public function model(array $row)
    {
        if($row['fullName'] != "" && $row['publicEmail'] != "" || $row['contactPhoneNumber'] != ""){
            if($row['profileUrl'] != "undefined"){
                return new InstagramProfilScraper([
                    'url_profil' => $row['profileUrl'],
                    'profil_name' => $row['profileName'],
                    'full_name' => $row['fullName'],
                    'bio' => $row['bio'],
                    'follower' => $row['followersCount'],
                    'following' => $row['followingCount'],
                    'instagram_id' => $row['instagramID'],
                    'is_business_account' => $row['isBusinessAccount'],
                    // 'business_category' => $row['businessCategory'],
                    'phone_number' => $row['contactPhoneNumber'],
                    'is_private' => $row['isPrivate'] == 'true' ? 1 : 0,
                    'is_verrified' => $row['isVerified'] == 'true' ? 1 : 0,
                    'email' => $row['publicEmail'],
                    'website' => $row['website'],
                    'emiten_id' => $this->penerbitId
                ]);
            }
        }
    }
}
