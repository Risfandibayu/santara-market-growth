<?php

namespace App\Imports;

use App\Models\FacebookProfilScraper;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ProfilFacebookImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    protected $penerbitId;

    public function __construct($penerbitId) {
        $this->penerbitId = $penerbitId;
    }


    public function model(array $row)
    {
        return new FacebookProfilScraper([
            'full_name' => $row['fullName'],
            'location_name' => $row['locationName'],
            'location_type' => $row['locationType'],
            'location_url' => $row['locationUrl'],
            // 'facebook' => $row['followersCount'],
            // 'gender' => $row['followingCount'],
            // 'interested_in' => $row['instagramID'],
            // 'languages' => $row['isBusinessAccount'],
            // 'bio' => $row['contactPhoneNumber'],
            'profile_picture_url' => $row['profilePictureUrl'],
            'facebook_id' => $row['facebookId'],
            'profile_url' => $row['profileUrl'],
            'emiten_id' => $this->penerbitId
        ]);
    }
}
