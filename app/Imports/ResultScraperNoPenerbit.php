<?php

namespace App\Imports;

use App\Models\InstagramProfilScraper;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ResultScraperNoPenerbit implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        if($row['fullName'] != "" && $row['publicEmail'] != "" && $row['contactPhoneNumber'] != ""){
            if($row['profileUrl'] != "undefined"){
                $cekEmail = InstagramProfilScraper::where('email', $row['publicEmail'])->count();
                if($cekEmail == 0){
                    return new InstagramProfilScraper([
                        'url_profil' => $row['profileUrl'],
                        'profil_name' => $row['profileName'],
                        'full_name' => $row['fullName'],
                        'bio' => $row['bio'],
                        'follower' => is_string($row['followersCount']) == 1 ? null : $row['followersCount'],
                        'following' => is_string($row['followingCount']) == 1 ? null : $row['followingCount'],
                        'instagram_id' => is_string($row['instagramID']) == 1 ? null : $row['instagramID'],
                        'is_business_account' => $row['isBusinessAccount'] == 'true' ? 1 : 0,
                        // 'business_category' => $row['businessCategory'],
                        'phone_number' => $row['contactPhoneNumber'],
                        'is_private' => $row['isPrivate'] == 'true' ? 1 : 0,
                        'is_verrified' => $row['isVerified'] == 'true' ? 1 : 0,
                        'email' => $row['publicEmail'],
                        'website' => $row['website'],
                        'scrapered_at' => date('Y-m-d H:i:s', strtotime($row['timestamp']))
                    ]);
                }
            }
        }
    }

}
